interface EasterItem {
  name: string,
  icon: string,
  quotes: string[]
}

class EasterCore {
  items: EasterItem[] = [
    // HL
    // EN 1
    {
      name: 'Gawr Gura',
      icon: '🦈',
      quotes: [
        'a',
        'Join the shark side.',
        'I\'m George?',
        'What\'s 9 + 10? 11.',
        '1, 2, 5? 1, 2, 6? Where\'s the rest of them?'
      ]
    },
    {
      name: 'Mori Calliope',
      icon: '💀',
      quotes: [
        'GUH',
        '*sips cheap wine*',
        'You\'re F\'n weird, kusotori.',
        'I will now show you my body!',
        'Excuse my rudeness, but could you please RIP?',
        'Very, very, very hard. Very rigind and very lo- *gulp*. Very long.'
      ]
    },
    {
      name: 'Amelia Watson',
      icon: '🔍',
      quotes: [
        '*hic*',
        'I pat the cactus thinking it\'d be fluffy.',
        '*injury count increases*',
        'Um... *ahem* Well... When I watch. Uh-when I coo-c- When I consume- when I uh. Pa- When I see- um. He-he-hent- When I see ART. Per-personally. Uh y\'know the big oooooolllllllll badonke-vlvlthebigol\' ti-ebilibl-big-b- ke-vl- Uh I\'m not a huge fan of the y\'know. Uhhh big. Uh. Bi-uh *ahem* si-size of- *ahem* uh. Uh size of L uheh *ahem* Ahhm I like \'em sma-smaller. Yeah. Not too- Not too sm... Ok. Next question.'
      ]
    },
    {
      name: 'Ninomae Ina\'nis',
      icon: '🐙',
      quotes: [
        'Wah!',
        'PLEASE STEP ON ME'
      ]
    },
    {
      name: 'Takanashi Kiara',
      icon: '🐔',
      quotes: [
        'Kikkeriki!',
        'Willst du Stress Mann?!',
        'Oida!',
        'Loaded with 2 pieces of original recipe popcorn chicken crispy strips, fries, salad and a drink.',
        'You know the rules and so do I.'
      ]
    },
    // IRyS
    {
      name: 'IRyS',
      icon: '💎',
      quotes: [
        'The baby is nice and delicious.',
        'It\'s not "ARRRSE"!',
        '*ceiling fan fearing sounds*',
        'How did I wet this whole building?',
        'Did... did I kill the baby?',
        'Yabairys.'
      ]
    },
    // JP 0
    {
      name: 'Tokino Sora',
      icon: '⭐',
      quotes: [
        '*breaks neck*'
      ]
    },
    {
      name: 'Roboco',
      icon: '🤖',
      quotes: [
        'Haro~bo~'
      ]
    },
    {
      name: 'Sakura Miko',
      icon: '💮',
      quotes: [
        'FAQ',
        'Nya-hello~!',
        'Elite'
      ]
    },
    {
      name: 'Hoshimachi Suisei',
      icon: '☄️',
      quotes: [
        'Suicopath.',
        'Yihihihi!'
      ]
    },
    {
      name: 'AZKi',
      icon: '🍑',
      quotes: [
        '🔑'
      ]
    },
    // JP 1
    {
      name: 'Yozora Mel',
      icon: '🦇',
      quotes: [
        'Banpire',
        'Kapu, kapu~'
      ]
    },
    {
      name: 'Shirakami Fubuki',
      icon: '🌽',
      quotes: [
        'No, no wife! Friend, friend!',
        'Dokomitendayo?!',
        '*eurobeat*'
      ]
    },
    {
      name: 'Natsuiro Matsuri',
      icon: '🏮',
      quotes: [
        'I wore only bandaids to school.',
        '🟦',
        'I swear she looked over 9000!',
        'I am GOD, ok?'
      ]
    },
    {
      name: 'Aki Rosenthal',
      icon: '🍎',
      quotes: [
        '*hips*',
        '*sings Russian, Russianly*'
      ]
    },
    {
      name: 'Akai Haato',
      icon: '❤️',
      quotes: [
        `*on ${this.randomOrderNumber()} hiatus*`,
        'HAACHAMACHAMAAAAAAA~!',
        'Who\'s Haato?'
      ]
    },
    // JP 2
    {
      name: 'Minato Aqua',
      icon: '⚓',
      quotes: [
        'Baqua',
        'kiyababavowowowaowuveyuburidondududeeeh',
        'NEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE!?'.split('').join(' ')
      ]
    },
    {
      name: 'Murasaki Shion',
      icon: '🌙',
      quotes: [
        'NEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE!?'.split('').join(' '),
        'Kusogaki'
      ]
    },
    {
      name: 'Nakiri Ayame',
      icon: '😈',
      quotes: [
        '*giggles*',
        'Yo dayo!',
        'Yo dazo!',
        'Dochi, dochi~'
      ]
    },
    {
      name: 'Yuzuki Choco',
      icon: '🍫',
      quotes: [
        '80085',
        'Ara, ara~',
        '<insert ASMR here>'
      ]
    },
    {
      name: 'Oozora Subaru',
      icon: '🦆',
      quotes: [
        'Shuba, shuba!',
        'Subaru is a girl, you know?'
      ]
    },
    // JP GAMERS (Minus FBK)
    {
      name: 'Ookami Mio',
      icon: '🐺',
      quotes: [
        'Faqing cute wulfu!',
        'MYOOOOOOOOON!',
        '*motherly noises*',
        '*motherly giggles*',
        '*adopts another cat*',
        '*tells you about the benefits of sleeping naked*'
      ]
    },
    {
      name: 'Nekomata Okayu',
      icon: '🍙',
      quotes: [
        'Mogu, mogu~!',
        'Okazu',
        '*teetee with Korone*'
      ]
    },
    {
      name: 'Inugami Korone',
      icon: '🥐',
      quotes: [
        'Yubi, yubi!',
        'I am Volks Wagen!',
        'I am Mercedes Benz!',
        'Kiss me Mario!',
        '*teetee with Okayu*',
        'Have confidence! ...no confidence!',
        '*endurances your endurance stream*',
        'DOOG',
        'Bonk!',
        'AAAAAAAAAAAAAAAH!... Soap...',
        'Oh, I\'m die, thank you fo\'evah...'
      ]
    },
    // JP 3
    {
      name: 'Usada Pekora',
      icon: '🐇',
      quotes: [
        'Konpeko, konpeko!',
        'Pe ↘️ ko ↗️ Pe ↘️ ko ↗️ Pe ↘️ ko ↗️!',
        '*adds more explosives*',
        '*commits war crimes*',
        'Almondo, almondo!',
        'HEY MOONA!',
        '-peko'
      ]
    },
    {
      name: 'Uruha Rushia',
      icon: '🦋',
      quotes: [
        '*untranscribable scream*',
        'RUSHIA IS BOING BOING!!!',
        '*gets cut on*',
        '*petan noises*',
        '<-10 dmg to Desk-kun>'
      ]
    },
    {
      name: 'Shiranui Flare',
      icon: '🔥',
      quotes: [
        'If you fap to me, do it quietly.'
      ]
    },
    {
      name: 'Shirogane Noel',
      icon: '⚔️',
      quotes: [
        'K cup',
        'I\'m gonna beat yo\' ass with my mace.',
        '*throws a dragon into orbit*'
      ]
    },
    {
      name: 'Houshou Marine',
      icon: '🏴‍☠️',
      quotes: [
        'Ahoy!',
        'I\'m horny!',
        'Cool! Dope! Super lit!',
        'My english is not 0, but 1!'
      ]
    },
    // JP 4
    {
      name: 'Amane Kanata',
      icon: '🦍',
      quotes: [
        'Hei!',
        'Souran, souran!',
        'Dokoisho, dokoisho!'
      ]
    },
    {
      name: 'Kiryu Coco',
      icon: '🐉',
      quotes: [
        'Good morning, motherfuckers!',
        'Juan',
        '-el puta!',
        '*inserts tail*',
        'Uuuuwawawawa~!',
        'Gomenasorry!',
        'Big kusa...',
        'Yametekudastop!'
      ]
    },
    {
      name: 'Tsunomaki Watame',
      icon: '🐑',
      quotes: [
        'Dododooo!',
        'Zzzzzz...',
        'I\'m... not at fault, right?',
        '*disconnects*'
      ]
    },
    {
      name: 'Tokoyami Towa',
      icon: '👼',
      quotes: [
        'Konyappi!'
      ]
    },
    {
      name: 'Himemori Luna',
      icon: '👶',
      quotes: [
        'Nanora!',
        'I\'ll punch you in the face nanora.'
      ]
    },
    // JP 5
    {
      name: 'Yukihana Lamy',
      icon: '☃️',
      quotes: [
        '*gulp*'
      ]
    },
    {
      name: 'Momosuzu Nene',
      icon: '🦭',
      quotes: [
        'Matanene!'
      ]
    },
    {
      name: 'Shishiro Botan',
      icon: '🦁',
      quotes: [
        'Poi!'
      ]
    },
    {
      name: 'Omaru Polka',
      icon: '🤡',
      quotes: [
        'POLMAO'
      ]
    },
    {
      name: 'Mano Aloe',
      icon: '👅',
      quotes: [
        '*debuts, refuses to elaborate, leaves*'
      ]
    },
    // ID 1
    {
      name: 'Ayunda Risu',
      icon: '🐿️',
      quotes: [
        'NUT!',
        'Big tasty nuts!',
        'Give me your nut!',
        'NNN - Non-stop Nut November',
        'I\'m in danger.'
      ]
    },
    {
      name: 'Moona Hoshinova',
      icon: '🔮',
      quotes: [
        '*dolphin noises*',
        'Is the BGM too big?'
      ]
    },
    {
      name: 'Airani Iofifteen',
      icon: '🎨',
      quotes: [
        'Erofi',
        `*chews you out in ${Math.floor(Math.random() * 15) + 5} languages*`
      ]
    },
    // ID 2
    {
      name: 'Kuraiji Ollie',
      icon: '🧟‍♀️',
      quotes: [
        'I peed like a charge rifle!',
        'Math is like Apex.',
        '*lowers volume before playing Ollie\'s video*',
        'Hololive harem protagonist.',
        '*passes out*'
      ]
    },
    {
      name: 'Anya Melfissa',
      icon: '🍂',
      quotes: [
        '👀',
        'Bananya.',
        'This... design shiet...',
        'Its smug aura mocks me.'
      ]
    },
    {
      name: 'Pavolia Reine',
      icon: '🦚',
      quotes: [
        '*car engine choking sounds*',
        'Fishing is my pashion!',
        '*munch, munch, munch...',
        'Have you seen my melons?'
      ]
    }
  ]

  constructor () {
    this.items.sort((a, b) => {
      if (a.name < b.name) {
        return -1
      } else if (a.name > b.name) {
        return 1
      } else {
        return 0
      }
    })
  }

  pick (): EasterItem {
    const roll = Math.floor(Math.random() * this.items.length)
    return this.items[roll]
  }

  randomOrderNumber (): string {
    const number = Math.floor(Math.random() * 900) + 100
    const strNumber = number.toString()
    const final = strNumber[strNumber.length - 1]
    let ending = 'th'
    if (final === '1') {
      ending = 'st'
    } else if (final === '2') {
      ending = 'nd'
    } else if (final === '3') {
      ending = 'rd'
    }
    return `${number}${ending}`
  }
}

export default EasterCore
